
import UIKit

class ListVC : UIViewController{

    @IBOutlet var listTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addAction(sender: AnyObject) {
        let detailView =  self.storyboard?.instantiateViewControllerWithIdentifier(DETAILVC) as! DetailVC
        self.presentViewController(detailView, animated: true, completion: nil)
    }
}
